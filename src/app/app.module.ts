import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { ServiceListComponent } from './services/service-list.component';
import { ServiceModule } from './services/service.module';

const appRoutes: Routes = [
  { path: '', component: ServiceListComponent },
  { path: 'services', component: ServiceListComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    ServiceListComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ServiceModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
