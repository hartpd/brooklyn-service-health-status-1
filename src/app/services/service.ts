import * as moment from 'moment';

export interface Service {
    url: string;
    isActive: boolean;
    activeDateTime: Date;  
    timeDifference: number; 
}